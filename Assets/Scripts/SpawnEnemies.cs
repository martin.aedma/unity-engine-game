using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    // This Script handles spawning enemies

    public GameObject enemie;

    private int m_counter;
    private float m_timer;
    private Vector3 m_one = new Vector3(42.0f, 0.25f, 14.0f);
    private Vector3 m_two = new Vector3(42.0f, 0.25f, 2.0f);
    private Vector3 m_three = new Vector3(-5.0f, 0.25f, 14.0f);
    private Vector3 m_four = new Vector3(-5.0f, 0.25f, 2.0f);

    void Start()
    {
        m_timer = 1.0f;
    }

    void Update()
    {
        m_timer -= Time.deltaTime;
        if (m_timer < 0.0f)
        {
            SpawnCreature();
            m_timer = 1.0f;
        }
    }

    void SpawnCreature()
    {
        m_counter = Random.Range(1, 5);


        Vector3 spawnPoint;
        switch (m_counter)
        {
            case 1:
                spawnPoint = m_one;
                break;
            case 2: 
                spawnPoint = m_two;
                break;
            case 3:
                spawnPoint = m_three;
                break;
            case 4:
                spawnPoint = m_four;
                break;
            default:
                spawnPoint = m_one;
                break;
        }

        Instantiate(enemie, spawnPoint, Quaternion.identity);
    }
}
