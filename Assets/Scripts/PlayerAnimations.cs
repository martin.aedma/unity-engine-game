using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    // This Script Controls Player animations

    private Animator m_PlayerAnimator;    

    void Start()
    {
        m_PlayerAnimator = GetComponent<Animator>();        
    }

    // Forward one running animation, every other direction uses some hovering like animation.
    // I did not make assets so I took free ones and used what I got as optimal as possible
    void Update()
    {
        if(Input.GetKey(KeyCode.W))
        {
            m_PlayerAnimator.SetBool("isRunning", true);
        } else
        {
            m_PlayerAnimator.SetBool("isRunning", false);
        }

        if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.S))
        {
            m_PlayerAnimator.SetBool("isStrafeing", true);
        } else
        {
            m_PlayerAnimator.SetBool("isStrafeing", false);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            m_PlayerAnimator.SetTrigger("isAttacking");
        }
        else
        {
            m_PlayerAnimator.ResetTrigger("isAttacking");
        }

    }
}
