using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour
{
    // Sphere script is for powerhit "Deathball" effect

    public GameObject ball;

    private LineRenderer m_lineRenderer;

    // Everything Deathball does occurs immediately at instantiation
    // check if there are nearby Creatures, at radius of 6.0f
    // if there are, kill them and spawn another Deathball

    void Start()
    {
        m_lineRenderer = gameObject.AddComponent<LineRenderer>();
        m_lineRenderer.positionCount = 2;
        m_lineRenderer.startWidth = 0.15f;
        m_lineRenderer.endWidth = 0.15f;
        m_lineRenderer.startColor = Color.green;
        m_lineRenderer.endColor = Color.green;

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 6.0f);
        foreach(var collider in hitColliders)
        {
            if(collider.tag == "Creature")
            {
                CreatureController controller = collider.transform.root.gameObject.GetComponent<CreatureController>();

                // Check only alive creatures that are nearby
                if (controller.isThisAlive())
                {
                    m_lineRenderer.SetPosition(0, transform.position);
                    m_lineRenderer.SetPosition(1, controller.transform.position + new Vector3(0.0f, 2.0f, 0.0f));
                    controller.Death(true);
                }
            }
        }
    } 
}
