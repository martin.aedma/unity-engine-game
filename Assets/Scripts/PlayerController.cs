using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    // Player logic, movements, rotation and UI screen switch for game start / end

    public Canvas startScreen;
    public Canvas uiScreen;
    public Canvas endScreen;
    public TMPro.TextMeshProUGUI finalText;
    public Controls controls;
    public float runSpeed, strafeSpeed;
    public float mouseHorizontalSpeed = 2.0F;
    public float mouseVerticalSpeed = 4.0F;
    public GameObject followTarget;
    public SkinnedMeshRenderer dodgeSkin;
    public Material yellow;
    public Material red;

    private bool m_isDodgeOn;
    private bool m_isDodgePossible;
    private bool m_isInvulnerable;
    private bool m_gameStarted;
    private bool m_gameOver;
    private Vector2 m_inputs;
    private Vector3 m_velocity;
    private CharacterController m_characterController;
    private DodgeBar m_dodgeBar;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        m_characterController = GetComponent<CharacterController>();
        m_dodgeBar = GetComponent<DodgeBar>();
        m_gameOver = false;
        m_gameStarted = false;
        m_isDodgeOn = false;
        m_isDodgePossible = true;
        m_isInvulnerable = false;
        StartGame();
    }

    public bool InvulnerableStatus()
    {
        return m_isInvulnerable;
    }

    void StartGame()
    {
        Time.timeScale = 0;
        startScreen.enabled = true;
        endScreen.enabled = false;
        uiScreen.enabled = false;
    }

     void Update()
    {
        Rotation();

        // Game is Over
        if (m_gameOver)
        {
            if (Input.GetKeyDown(KeyCode.N))
            {
                Time.timeScale = 1;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }

        // Game Before Start
        if(!m_gameStarted)
        {
            // Start Game
            if (Input.GetKeyDown(KeyCode.N))
            {
                Time.timeScale = 1;
                startScreen.enabled = false;
                uiScreen.enabled = true;
            }
        }
    }

    public void GameOver(int number)
    {
        m_gameOver = true;
        Time.timeScale = 0;
        uiScreen.enabled = false;
        endScreen.enabled = true;
        finalText.text = "FINAL SCORE : " + number.ToString() + "\n PRESS N TO START";
    }

    void FixedUpdate()
    {
        GetInputs();
        Movement();
    }

   void Rotation()
    {
        float h = mouseHorizontalSpeed * Input.GetAxis("Mouse X");
        transform.Rotate(0, h, 0);
    }

    void Movement()
    {
        Vector2 inputNormalized = m_inputs;

        m_velocity = (transform.forward * inputNormalized.y + transform.right * inputNormalized.x) * Time.deltaTime;

        if (m_inputs.y == 1 && m_inputs.x == 0)
        {
            m_velocity *= runSpeed;           
        } else
        {
            if (m_isDodgeOn)
             {
                m_velocity *= strafeSpeed * 5.0f;
                m_isDodgePossible = false;                
                m_isInvulnerable = true;

                StartCoroutine(CoRoutine(0.3f, () =>
                {
                    m_isDodgeOn = false;
                }));

                StartCoroutine(CoRoutine(3.0f, () =>
                {
                    m_isInvulnerable = false;
                    dodgeSkin.material = yellow;
                }));

                StartCoroutine(CoRoutine(5.0f, () =>
                {
                    m_isDodgePossible = true;
                }));

            } else
            {
                m_velocity *= strafeSpeed;
            }                      
        }

        m_characterController.Move(m_velocity);
    }

    void GetInputs()
    {
        // Y-AXIS MOVEMENT forward : backward
        // forward
        if(Input.GetKey(controls.forwards))
        { 
            m_inputs.y = 1;
        }

        // backward
        if (Input.GetKey(controls.backwards)) {
            if(Input.GetKey(controls.forwards))
            {
                m_inputs.y = 0;
            } else
            {
                m_inputs.y = -1;
            }
        }

         // no input on y-axis
        if (!Input.GetKey(controls.forwards) && !Input.GetKey(controls.backwards)){
            m_inputs.y = 0;
        }

        // X-AXIS MOVEMENT strafe left : right
        // strafe left
        if (Input.GetKey(controls.strafeLeft))
        {
            if (Input.GetKey(controls.dodge) && m_isDodgePossible)
            {
                m_inputs.x = -1;
                m_isDodgeOn = true;
                dodgeSkin.material = red;
                m_dodgeBar.StartFillingDodge();
            } else
            {
                m_inputs.x = -1;
            }
            
        }

        // strafe right
        if (Input.GetKey(controls.strafeRight))
        {
            if (Input.GetKey(controls.strafeLeft))
            {
                m_inputs.x = 0;
            }
            else if(Input.GetKey(controls.dodge) && m_isDodgePossible)
            {
                m_inputs.x = 1;
                m_isDodgeOn = true;
                dodgeSkin.material = red;
                m_dodgeBar.StartFillingDodge();
            }
            else
            {
                m_inputs.x = 1;
            }
        }

        // no input on x-axis
        if (!Input.GetKey(controls.strafeLeft) && !Input.GetKey(controls.strafeRight))
        {
            m_inputs.x = 0;
        }
    }
    IEnumerator CoRoutine(float delayed, System.Action action)
    {
        yield return new WaitForSeconds(delayed);
        action();
    }
}
