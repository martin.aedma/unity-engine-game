using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureSlowed : MonoBehaviour
{
    public Material green;

    private SkinnedMeshRenderer m_meshRenderer;

    private void Start()
    {
        m_meshRenderer = GetComponent<SkinnedMeshRenderer>();
    }

    public void SwitchColor()
    {
        m_meshRenderer.material = green;
    }
}
