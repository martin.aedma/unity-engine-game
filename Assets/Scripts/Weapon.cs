using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    // Weapon script handles a lot of game logic. Hitting creatures especially

    public AudioClip swing;
    public AudioClip hit;
    public AudioClip powerHit;
    public TMPro.TextMeshProUGUI powerTxt; 
    public Image swordIcon;
    public Sprite zero;
    public Sprite one;
    public Sprite two;
    public Sprite three;    
    public Material powerful;

    private Material m_basic;
    private BoxCollider m_swordCollider;
    private bool m_hitPossible;
    private Animator m_playerAnim;
    private int m_powerCount;
    private bool m_powerHit;
    private Score m_scoreReference;
    private AudioSource m_audioSource;
  

    void Start()
    {
        m_scoreReference = gameObject.transform.root.gameObject.GetComponent<Score>();
        m_swordCollider = GetComponent<BoxCollider>();
        m_audioSource = GetComponent<AudioSource>();
        m_playerAnim = transform.root.gameObject.GetComponent<Animator>();
        m_basic = GetComponent<SkinnedMeshRenderer>().material;

        m_swordCollider.enabled = false;
        m_hitPossible = true;
        m_powerHit = false;
        m_powerCount = 0;
        swordIcon.sprite = zero;
        powerTxt.text = "0/3";
    }

    public void PlaySound(AudioClip clip)
    {
        m_audioSource.PlayOneShot(clip);
    }

    void Update()
    {
       // Attack Collison mecanic is made so that BoxCollider on Weapon is enabled for a brief moment during attack animation
       // Every 1.1 seconds hit is possible, this avoids spamming attack button to constantly trigger hit.
       // This mecanic is not perfect, I tried several things but did not get it perfectly smooth. There will be occasionally glitches
       // like sword is not in attack animation but kills occur anyway. And visa vers, sometimes it seems you attack creature but kill
       // does not occur. Should have more time to work out better system.

        if (Input.GetKeyDown(KeyCode.E) && (m_hitPossible))
        {
            m_hitPossible = false;
            PlaySound(swing);            

            StartCoroutine(CheckingSwordHit(0.6f, () =>
            {
                AnimatorClipInfo[] clipInfo = m_playerAnim.GetCurrentAnimatorClipInfo(0);

               if  (clipInfo[0].clip.length > 1.3f && clipInfo[0].clip.name == "Attack1" )
                {                        
                    m_swordCollider.enabled = true;
                }                               
            }));

            StartCoroutine(CheckingSwordHit(0.7f, () =>
            {
                m_swordCollider.enabled = false;
            }));

            StartCoroutine(CheckingSwordHit(1.1f, () =>
            {
                // Get Number of Creatures killed by Powerhit by counting number of Lazer Spheres in scene
                int multiplier = GameObject.FindGameObjectsWithTag("Lazer").Length;
                if (multiplier > 0)
                {               
                    m_scoreReference.AddToScore(multiplier);
                }
                m_hitPossible = true;
            }));
        }
    }

    // Count hits if collided with creature
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Creature")
        {
            PlaySound(hit);

            if (!m_powerHit) {
                // score for single hits, without powerhit
                m_scoreReference.AddToScore(1);                
            } else
            {
                PlaySound(powerHit);
            }

            GameObject creature = other.gameObject.transform.root.gameObject;
            creature.GetComponent<CreatureController>().Death(m_powerHit);

            if (!m_powerHit)
            {
                m_powerCount++;
            }
            
            m_powerHit = false;
            GetComponent<SkinnedMeshRenderer>().material = m_basic;

            // if power reaches 3 then turn on powerhit and switch blade material
            if (m_powerCount >= 3)
            {
                m_powerHit = true;
                GetComponent<SkinnedMeshRenderer>().material = powerful;
                m_powerCount = 0;
            }


            // switch icons on UI according to power level
            if (m_powerHit)
            {
                swordIcon.sprite = three;
                powerTxt.text = "3/3";
            } else
            {
                powerTxt.text = m_powerCount.ToString() + "/3";
                switch(m_powerCount)
                {
                    case 0:
                        swordIcon.sprite = zero;
                        break;
                    case 1:
                        swordIcon.sprite = one;
                        break;
                    case 2:
                        swordIcon.sprite = two;
                        break;
                    default:
                        swordIcon.sprite = zero;
                        break;                        
                }
            }
        }
            
    }

    // Coroutine

    IEnumerator CheckingSwordHit(float delayed, System.Action action)
    {
        yield return new WaitForSeconds(delayed);
        action();
    }
   
}
