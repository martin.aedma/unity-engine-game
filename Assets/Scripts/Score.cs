using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    // Score script handles UI information of score, kills and time

    public TMPro.TextMeshProUGUI scoreTxt;
    public TMPro.TextMeshProUGUI killsTxt;
    public TMPro.TextMeshProUGUI timeTxt;

    private int m_score;
    private int m_kills;
    private int m_time;

    void Start()
    {
        m_kills = 0;
        m_score = 0;
        scoreTxt.text = "Score \n" + m_score.ToString();
        killsTxt.text = "Kills \n" + m_kills.ToString();
        timeTxt.text = "Time \n 0";
    }

    private void Update()
    {
        float t = Time.timeSinceLevelLoad;
        m_time = (int)Mathf.Floor(t);
        timeTxt.text = "Time \n" + m_time.ToString();
    }

    // Scoring logic, single kills award base 1 point + current time in seconds. Kills with Powerhit award base points per creature killed
    // but also add square of number of monsters killed during powerhit.
    
    public void AddToScore(int multiplier)
    {
        if(multiplier == 1)
        {
            m_kills++;
            m_score += 1 + m_time;
        } else
        {
            m_kills += multiplier;
            m_score += (multiplier * m_time) + multiplier * multiplier;
        }
        scoreTxt.text = "Score \n" + m_score.ToString();
        killsTxt.text = "Kills \n" + m_kills.ToString();
    }

    public void GameEnding()
    {
        GetComponent<PlayerController>().GameOver(m_score);
    }

}
