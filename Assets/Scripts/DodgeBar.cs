using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DodgeBar : MonoBehaviour
{
    public Slider slider;
    public Text dodgeText;
    private float m_currentValue = 0.0f;
    private bool m_StartFill;

    // Start is called before the first frame update
    void Start()
    {
        slider.value = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_StartFill)
        {
            m_currentValue += Time.deltaTime * 0.2f;
            slider.value = m_currentValue;
            if(m_currentValue > 1.0f)
            {
                m_StartFill = false;
            }
        }
    }

    public void StartFillingDodge()
    {
        m_StartFill = true;
        slider.value = 0f;
        m_currentValue = 0.0f;
    }
}
