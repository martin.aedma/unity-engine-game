using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Controls
{
    // Just made keyes public to editor in case I wanted to change them. This Script is on Player GameObject
    public KeyCode forwards, backwards, strafeLeft, strafeRight, attack, dodge;
}
