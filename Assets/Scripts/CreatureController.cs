using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureController : MonoBehaviour
{
    // CreatureController handles creature logic : movement, speed, death and animations

    public GameObject deathBall;
    public GameObject bodyMesh;
    public AudioClip aboutToAttack;    
    
    private Animator m_animator;
    private bool m_isAlive = true;
    private GameObject m_deathBall;
    private float creatureSpeed = 1.0f;
    private float m_distance;
    private AudioSource m_audioSource;
   

    public bool isThisAlive()
    {
        return m_isAlive;
    }
    
    // New creatures get spawned with increasing speed. With these settings game lasts about 1-1.5 mins
    void Start()
    {
        m_animator = GetComponent<Animator>();        
        m_audioSource = GetComponent<AudioSource>();
        creatureSpeed = 1.0f + Time.timeSinceLevelLoad * 0.05f;
    }

    public void PlaySound(AudioClip clip)
    {
        m_audioSource.PlayOneShot(clip);
    }

    void FixedUpdate()
    {
        if (m_isAlive)
        {
 
            Transform player = GameObject.Find("Player").transform;
            transform.LookAt(player);

            m_distance = Vector3.Distance(player.position, transform.position);

            // I tried a couple of ways to make player death logic with colliders but this was not so smooth.
            // I found it better to simply check distance between player and creater.
            // If creature gets close, they play attack animation and after 1 second there is check if player is still close.
            // If player stays close its over

            if (m_distance < 2.5f)
            {
                if (!m_audioSource.isPlaying)
                {
                    PlaySound(aboutToAttack);
                }

                if (player.GetComponent<PlayerController>().InvulnerableStatus())
                {
                    // SLOW CREATURE
                    creatureSpeed = 0.2f;

                    // Change Color
                    bodyMesh.GetComponent<CreatureSlowed>().SwitchColor();
                }

                StartCoroutine(CoRoutine(1.05f, () =>
                {
                   if (m_distance < 2.5f && m_isAlive)
                    {
                        
                        if (!player.GetComponent<PlayerController>().InvulnerableStatus())
                        {
                            // GAME OVER
                            player.GetComponent<Score>().GameEnding();
                        }                      
                    }
                                        
                }));

                StartCoroutine(CoRoutine(0.15f, () =>
                {
                    m_animator.SetBool("isClose", true);

                }));

                StartCoroutine(CoRoutine(1.95f, () =>
                {
                    m_animator.SetBool("isClose", false);

                }));

            }         

            CreatureMovement();
        }
        
    }

    void CreatureMovement()
    {
        transform.position += transform.forward * creatureSpeed * Time.deltaTime;
    }
    
    public void Death(bool powerUp)
    {
        m_isAlive = false;
        m_animator.SetBool("isDead", true);

        // If Death is triggered with powerUp it means its powerHit and Deathball will be instantiated
        if (powerUp)
        {
            m_deathBall = Instantiate(deathBall, transform.position + new Vector3(0.0f, 2.0f, 0.0f), Quaternion.identity);
        }          

        StartCoroutine(CoRoutine(1.35f, () =>
        {
            Destroy(gameObject);
            Destroy(m_deathBall);            
        }));
    }

    // Coroutine
    IEnumerator CoRoutine(float delayed, System.Action action)
    {
        yield return new WaitForSeconds(delayed);
        action();
    }   
}
